#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Description

© 2018 Paul Schroeder
"""

from .__version__ import __version__

__all__ = [
    "__title__",
    "__summary__",
    "__uri__",
    "__version__",
    "__author__",
    #"__email__",
    "__license__",
]

__title__ = "py7219"
__summary__ = "This module's purpose is to simplify the handling of a MAX7219/21 IC."
__uri__ = 'https://gitlab.com/milkpirate/py7219'

__author__ = "Paul Schroeder"
#__email__ = ""

__license__ = 'GPLv3'
