#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module is a wrapper to controll a MAX7219/7221 IC via SPI with a \
littleWire:

.. _littleWire:
   https://littlewire.github.io/

Todo:
    * Add more module description
    * Add usage examples

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Attributes:
    Max (class): The main class for creating instances to control the hardware.


© 2018 Paul Schroeder
"""


import littleWire


from types import SimpleNamespace


class Max(object):
    """
    Class for handling the the MAX7221/MAX7219 ICs via LittleWire. Creates \
an instance of the MAX7219 IC handler.

    :param lat_pin: pin 0, ..., 5 or littleWire.PINx (x=1, ..., 4) of the \
littleWire used for latching. by default it is set to littleWire.PIN3
    :type lat_pin: int

    :param common_anode: determines weather the wiring is command anode or \
cathode. Default is True for common anode.
    :type common_anode: bool

    :param number_of_digits: number of digits used (1, ...,8). Default is 8.
    :type number_of_digits: int

    :param lw: littleWire instance to be used (if none is given, a new one \
will be created)
    :type lw: littleWire.device

    Following variables are instance variables and settable via constructor:

    :ivar lat_pin: initial value: littleWire.PIN3
    :ivar common_anode: initial value: True
    :ivar number_of_digits: initial value: 8
    :ivar lw: initial value: littleWire.device()

    Following variables are class attributes:

    :cvar op_code: MAX7219 op-codes
    :type op_code: types.SimpleNamespace
    """

    # This class attribute is a namespace containing
    # all valid MAX7219 op-codes.
    op_code = SimpleNamespace(
        nop=0,
        digit0=1,
        digit1=2,
        digit2=3,
        digit3=4,
        digit4=5,
        digit5=6,
        digit6=7,
        digit7=8,
        decode_mode=9,
        intensity=10,
        scan_limit=11,
        shutdown=12,
        display_test=15,
    )

    #   _A_
    #  |   |
    #  F   B
    #  |-G-|
    #  E   C
    #  |_D_| oP

    # 0bABCDEFGP
    _char_lookup = {
        "0": 0b11111100,
        "1": 0b01100000,
        "2": 0b11011010,
        "3": 0b11110010,
        "4": 0b01100110,
        "5": 0b10110110,
        "6": 0b10111110,
        "7": 0b11100000,
        "8": 0b11111110,
        "9": 0b11110110,

        "A": 0b11101110,
        "C": 0b10011100,
        "E": 0b10011110,
        "F": 0b10001110,
        "G": 0b10111100,
        "H": 0b01101110,
        "I": 0b01100000,
        "J": 0b11111000,
        "L": 0b00011100,
        "P": 0b11001110,
        "S": 0b10110110,
        "U": 0b01111100,
        "Y": 0b01110110,
        "Z": 0b11011010,
        '"': 0b01000100,
        ".": 0b00000001,
        "-": 0b00000010,
        "^": 0b11000100,
        "_": 0b00010000,
        "b": 0b00111110,
        "c": 0b00011010,
        "d": 0b01111010,
        "h": 0b00101110,
        "i": 0b00100000,
        "j": 0b00110000,
        "l": 0b00011000,
        "n": 0b00101010,
        "o": 0b00111010,
        "r": 0b00001010,
        "t": 0b00011110,
        "u": 0b00111000,
    }

    for i in range(10):
        _char_lookup[i] = _char_lookup[str(i)]
    del i

    _char_lookup[10] = _char_lookup[0]
    _char_lookup["10"] = _char_lookup[0]
    _char_lookup["O"] = _char_lookup[0]

    _max_digits = 8

    def __init__(
        self,
        lat_pin=littleWire.PIN3,
        common_anode=False,
        number_of_digits=_max_digits,
        lw=None,
    ):
        # sanitize arguments
        (
            self.lat_pin,
            self.common_anode,
            self.number_of_digits,
            self.lw
        ) = self._sanitize_args(
            lat_pin,
            common_anode,
            number_of_digits,
            lw,
        )

        # prepare buffers
        self.frame_buffer = [0x00] * self._max_digits

        # setup littlewire
        self.lw.pinMode(self.lat_pin, littleWire.OUTPUT)
        self.lw.digitalWrite(self.lat_pin, littleWire.LOW)
        self.lw.spi_init()

        # init max72xx
        self.spi_transfer(self.op_code.display_test, 0)  # no display test
        self.spi_transfer(self.op_code.decode_mode, 0)   # no decode
        self.spi_transfer(self.op_code.shutdown, 1)      # no shutdown

        scan_limit = 7 if common_anode else number_of_digits - 1
        self.spi_transfer(self.op_code.scan_limit, scan_limit)

        self.brightness = 1
        self.set_brightness(self.brightness)

        self.clear()

    def _sanitize_args(self, lat_pin, common_anode, number_of_digits, lw):
        """
        Sanitizes constructor arguments.

        :param lat_pin: see py7219.Max()
        :param common_anode: see py7219.Max()
        :param number_of_digits: see py7219.Max()
        :param lw: see py7219.Max()

        :type lat_pin: int
        :type common_anode: bool
        :type number_of_digits: int
        :type lw: littleWire.device

        :raises TypeError: not an int, not a bool or not a littleWire device
        :raises IndexError: out of range
        """

        if not isinstance(lat_pin, int):
            raise TypeError("not an int")
        if lat_pin not in range(6):
            raise IndexError("out of range")

        # littleWires pin mess
        lat_pin = 0 if lat_pin == 4 else lat_pin
        lat_pin = 5 if lat_pin == 3 else lat_pin

        # polarity
        if common_anode not in [True, False, 0, 1]:
            raise TypeError("not a bool")

        # number of digits
        if not isinstance(number_of_digits, int):
            raise TypeError("not an int")
        if number_of_digits not in range(1, self._max_digits + 1):
            raise IndexError("out of range")

        lw = lw if lw else littleWire.device()

        # if we have a littleWire instance
        if not isinstance(lw, littleWire.device) and \
           not "device" == lw.__class__.__name__:
            raise TypeError("not a littleWire device")

        return lat_pin, common_anode, number_of_digits, lw

    def set_digit(self, digit, value):
        """
        Sets the digit to value. The values is also written to the internal \
frame buffer.

        :param digit: to write to (1,...8)
        :type digit: int

        :param value: ist the raw value to be written
        :type value: int

        :return: the MAX instance
        :rtype: py7219.Max

        :raises ValueError: not byte sized
        :raises TypeError: not an int
        :raises IndexError: out of range
        """

        if not isinstance(digit, int):
            raise TypeError("not an int")
        if digit not in range(1, self.number_of_digits + 1):
            raise IndexError("out of range")
        if not isinstance(value, int):
            raise TypeError("not an int")
        bytes([value])  # raises ValueError

        if self.common_anode:
            self.frame_buffer[digit % 8] = value
        else:
            self.frame_buffer[digit - 1] = value

        if self.common_anode:
            out_buffer = self.transpose(self.frame_buffer)
            for dig in range(1, self._max_digits+1):
                self.spi_transfer(dig, out_buffer[dig-1])
        else:
            self.spi_transfer(digit, value)

        return self

    def get_digit(self, digit):
        """
        Returns the value of a digit from the internal frame buffer.

        :param digit: to return the value from (1,...8)
        :type digit: int

        :return: value of frame buffer at the digit position
        :rtype: int

        :raises TypeError: not an int
        :raises IndexError: out of range
        """

        if not isinstance(digit, int):
            raise TypeError("not an int")
        if digit not in range(1, self.number_of_digits + 1):
            raise IndexError("out of range")

        return self.frame_buffer[digit - 1]

    def set_char(self, digit, char, dp=False):
        """
        Writes a character to MAX7219 IC at a specific position. Decimal \
point is optional. If the character is not displayable by the 7-segment \
display the corresponding digit will be blank and the corresponding frame \
buffer entry will be cleared.

        :param digit: desired position (1,...,8)
        :type digit: int

        :param char: character to be written
        :type char: int

        :param dp: determines if the decimal point on will ne shown. \
Optional parameter, default is False.
        :type dp: bool

        :return: the MAX instance
        :rtype: py7219.Max

        :raises UnicodeEncodeError: not an ascii charakter
        :raises TypeError: not an int, not a bool or not a str
        :raises IndexError: wrong str length or out of range
        """

        if not isinstance(digit, int):
            raise TypeError("not an int")
        if not isinstance(char, str):
            raise TypeError("not a str")
        if digit not in range(1, self.number_of_digits + 1):
            raise IndexError("out of range")
        if len(char) != 1:
            raise IndexError("wrong str length")
        if dp not in [True, False, 0, 1]:
            raise TypeError("not a bool")

        char.encode('ascii')  # raises UnicodeEncodeError

        out_byte = self._char_lookup.get(char, 0x00)
        if dp:
            out_byte |= self._char_lookup.get('.', 0x00)

        self.set_digit(digit, out_byte)
        return self

    def set_brightness(self, brightness):
        """
        Sets the brightness/duty cycle of the output.

        :param brightness: between 0, ..., 15 where 15 is the brightest \
and 0 the dimmest
        :type brightness: int

        :return: the MAX instance
        :rtype: py7219.Max

        :raises TypeError: not an int
        :raises IndexError: out of range
        """
        if not isinstance(brightness, int):
            raise TypeError("not an int")
        if brightness not in range(16):
            raise IndexError("out of range")

        self.brightness = brightness
        self.spi_transfer(self.op_code.intensity, brightness)
        return self

    def write_string(self, string=None):
        """
        Writes a string the MAX IC. If no, empty or a string consisting \
only of spaces is given, it behaves like clear().

        :param string: String to be written to the MAX7219 digits
        :type string: str

        :return: the MAX instance
        :rtype: py7219.Max

        :raises TypeError: not a str
        """

        if not string:
            self.clear()
            return self

        if not isinstance(string, str):
            raise TypeError("not a str")

        string += "\x00" * self.number_of_digits
        string = string[:self.number_of_digits]
        string = string[::-1]

        digit = 1
        for letter in string:
            if letter != "\x00":
                self.set_char(digit, letter)
            digit += 1

        return self

    def clear(self):
        """
        Clears the MAX7219 ICs output (in common anode mode the internal \
frame buffer will be cleared as well).

        :return: the MAX instance
        :rtype: py7219.Max
        """
        for i in range(1, 9):
            self.spi_transfer(i, 0x00)

        self.frame_buffer = [0x00] * self._max_digits
        return self

    @staticmethod
    def transpose(array):
        """
        Transposes mathematically a 8x8 bit matrix. The matrix is given by an \
array of 8 bytes representing the rows of it.

        :param array: list of 8 bytes
        :type array: list

        :return: transposed list (also of length 8)
        :rtype: list

        :raises ValueError: list does contain several types or not byte sized
        :raises TypeError: not a list
        :raises IndexError: wrong list length

        >>> Max.transpose([255, 0, 0, 0, 0, 0, 0, 0]) # doctest: +ELLIPSIS
        [128, ..., 128]
        >>> Max.transpose([0, 0, 0, 0, 0, 0, 0, 255]) # doctest: +ELLIPSIS
        [1, ..., 1]
        >>> Max.transpose([236, 37, 10, 189, 152, 250, 158, 9])
        [158, 132, 212, 30, 191, 210, 38, 81]
        """
        if not isinstance(array, list):
            raise TypeError("not a list")
        if not all(isinstance(x, int) for x in array):
            raise ValueError("list does contain several types")
        if len(array) != 8:
            raise IndexError("wrong list length")
        for data in array:
            bytes([data])  # raises ValueError

        ret = [0x00] * 8

        for col in range(8):
            for row in range(8):
                ret[row] <<= 1
                if array[col] & (1 << row):
                    ret[row] |= 1

        return ret[::-1]    # reverse list

    def spi_transfer(self, op_code, data):
        """
        Transfer opdcode and data to MAX7219 IC with the appropriate latching.

        :param op_code: must be valid for MAX7219 IC
        :type op_code: int

        :param data: is the payload byte corresponding to the op-code
        :type data: int

        :return: the MAX instance
        :rtype: py7219.Max

        :raises TypeError: not an int or not a MAX7219 IC op-code
        :raises ValueError: not byte sized
        """
        if op_code not in vars(self.op_code).values():
            raise ValueError("not a MAX7219 IC op-code")
        if not isinstance(data, int):
            raise TypeError("not an int")
        bytes([data])  # raises ValueError

        self.lw.spi_sendMessageMulti([op_code, data], 2, littleWire.MANUAL_CS)
        # generate positive edge to trigger latching:
        self.lw.digitalWrite(self.lat_pin, littleWire.LOW)
        self.lw.digitalWrite(self.lat_pin, littleWire.HIGH)
        return self
