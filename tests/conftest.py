#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""


import os
import pytest
import sys

import mock_littleWire
sys.modules['littleWire'] = mock_littleWire

import py7219  # test subject


default_lat_pin = 0
default_common_anode = False
default_number_of_digits = 6


@pytest.fixture()
def is_travis(request):
    is_travis = os.environ.get("TRAVIS", False)  # returns "true"/"false"
    return is_travis == "true"


@pytest.fixture()
def max_instance(request):
    """
    Create a instance of MAX IC class with mocked littel wire device

    :return: py7219.Max() instance
    """

    def max_factory(*pargs, **kwargs):
        kwargs.setdefault('lw', mock_littleWire.device())
        kwargs.setdefault('common_anode', False)
        kwargs.setdefault('number_of_digits', 6)
        max_instance = py7219.Max(*pargs, **kwargs)
        return max_instance

    return max_factory


@pytest.fixture(params=range(256))
def byte_range(request):
    return request.param


@pytest.fixture(params=iter(vars(py7219.Max.op_code).values()))
def opcode_range(request):
    return request.param


@pytest.fixture(params=range(16))
def brightness_range(request):
    return request.param


@pytest.fixture(params=iter([True, False, 0, 1]))
def boolean_range(request):
    return request.param


@pytest.fixture(params=range(1, 9))
def pin_range(request):
    return request.param


@pytest.fixture(params=range(1, py7219.Max._max_digits + 1))
def digit_range(request):
    return request.param