#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author: Paul Schroeder
"""

import pytest

import py7219


@pytest.mark.parametrize("non_lat_pin, excpt", [
    pytest.param(-1,        IndexError),
    pytest.param(6,         IndexError),
    pytest.param("no int",  TypeError),
])
def test_constructor_lat_pin_exceptions(max_instance, non_lat_pin, excpt):
    with pytest.raises(excpt):
        max_instance(lat_pin=non_lat_pin)


@pytest.mark.parametrize("non_common_anode, excpt", [
    pytest.param(123,       TypeError),
    pytest.param("no bool", TypeError),
])
def test_constructor_common_anode_exceptions(max_instance, non_common_anode, excpt):
    with pytest.raises(excpt):
        max_instance(common_anode=non_common_anode)


@pytest.mark.parametrize("non_number_of_digits, excpt", [
    pytest.param(0,         IndexError),
    pytest.param(9,         IndexError),
    pytest.param("no int",  TypeError),
])
def test_constructor_number_of_digits_exceptions(max_instance, non_number_of_digits, excpt):
    with pytest.raises(excpt):
        max_instance(number_of_digits=non_number_of_digits)


def test_constructor_wrong_input_littleWire_exceptions(max_instance):
    with pytest.raises(TypeError):
        max_instance(lw=123)


def test_write_str_exception(max_instance):
    # exception tests
    max_ic = max_instance()
    with pytest.raises(TypeError):
        max_ic.write_string(123)


@pytest.mark.parametrize("brightness, excpt", [
    pytest.param(-1,        IndexError),
    pytest.param(16,        IndexError),
    pytest.param("no int",  TypeError),
])
def test_set_brightness_exceptions(max_instance, brightness, excpt):
    max_ic = max_instance()
    with pytest.raises(excpt):
        max_ic.set_brightness(brightness)


@pytest.mark.parametrize("digit_num, preset, digit, in_chr, dp, excpt", [
    pytest.param(8, [0x55] * 8, 0, "A",         False,      IndexError),
    pytest.param(6, [0x55] * 8, 7, "A",         False,      IndexError),
    pytest.param(5, [0x55] * 8, "no int", "A",  0,          TypeError),
    pytest.param(5, [0x55] * 8, 4, 123,         True,       TypeError),
    pytest.param(5, [0x55] * 8, 4, "Ab",        True,       IndexError),
    pytest.param(5, [0x55] * 8, 4, "",          1,          IndexError),
    pytest.param(5, [0x55] * 8, 4, "A",         "no bool",  TypeError),
    pytest.param(5, [0x55] * 8, 4, "á",         True,       UnicodeEncodeError),
])
def test_set_char_exceptions(max_instance, digit_num, preset, digit, in_chr, dp, excpt):
    max_ic = max_instance(number_of_digits=digit_num)
    max_ic.frame_buffer = preset

    with pytest.raises(excpt):
        max_ic.set_char(digit, in_chr, dp)

    assert max_ic.frame_buffer == preset, "frame buffer does not match pre-set"


@pytest.mark.parametrize("in_digit, excpt", [
    pytest.param(0,         IndexError),
    pytest.param(7,         IndexError),
    pytest.param("no int",  TypeError),
])
def test_get_digit_exceptions(max_instance, in_digit, excpt):
    max_ic = max_instance(number_of_digits=6)
    with pytest.raises(excpt):
        max_ic.get_digit(in_digit)


@pytest.mark.parametrize("in_digit, in_byte, excpt", [
    pytest.param(0,         0x55,       IndexError),
    pytest.param(7,         0x55,       IndexError),
    pytest.param("no int",  0x55,       TypeError),
    pytest.param(1,         "no int",   TypeError),
    pytest.param(3,         -1,         ValueError),
    pytest.param(3,         256,        ValueError),
])
def test_set_digit_exceptions(max_instance, in_digit, in_byte, excpt):
    max_ic = max_instance(number_of_digits=6)
    with pytest.raises(excpt):
        max_ic.set_digit(in_digit, in_byte)


@pytest.mark.parametrize("opcode, data, excpt", [
    pytest.param(-1,                        0x55,       ValueError),
    pytest.param(13,                        0x55,       ValueError),
    pytest.param(14,                        0x55,       ValueError),
    pytest.param(16,                        0x55,       ValueError),
    pytest.param(py7219.Max.op_code.nop,    -1,         ValueError),
    pytest.param(py7219.Max.op_code.nop,    256,        ValueError),
    pytest.param(py7219.Max.op_code.nop,    "no int",   TypeError),
])
def test_spi_transfer_exceptions(max_instance, opcode, data, excpt):
    max_ic = max_instance()

    with pytest.raises(excpt):
        max_ic.spi_transfer(opcode, data)


@pytest.mark.parametrize("inp, excp", [
    pytest.param("no list",                     TypeError),
    pytest.param([123, "abc", 2.5],             ValueError),
    pytest.param([0]*9,                         IndexError),
    pytest.param([0]*0,                         IndexError),
    pytest.param([0, 0, -1, 0, 0, 0, 0, 0],     ValueError),
    pytest.param([0, 0, 0, 0, 0, 256, 0, 0],    ValueError),
])
def test_transpose_exception(inp, excp):
    with pytest.raises(excp):
        py7219.Max.transpose(inp)
